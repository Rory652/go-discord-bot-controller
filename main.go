package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

// File Variables used for command line parameters
var (
	File  string
	Clear bool
)

func init() {
	flag.StringVar(&File, "f", "", "JSON File")
	flag.BoolVar(&Clear, "clear", true, "Clear Commands")
	flag.Parse()
}

func main() {
	if File == "" {
		fmt.Println("error, no file specific ('-f filename.json')")
		return
	}

	newBot, err := read(File)
	if err != nil {
		fmt.Println("error reading json file,", err)
		return
	}

	client, err := botInit(newBot)
	if err != nil {
		fmt.Println("error initializing bot", err)
	}

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	err = client.Close()
	if err != nil {
		return
	}
}
