module gitlab.com/Rory652/go-discord-bot-controller

go 1.16

require (
	github.com/bwmarrin/discordgo v0.23.2 // indirect
	// Will bring back soon - github.com/bwmarrin/discordgo v0.23.2
	github.com/rory652/discordgo v0.23.4
)
