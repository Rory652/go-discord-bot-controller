package main

import (
	"encoding/json"
	"fmt"
	"github.com/rory652/discordgo"
	"io/ioutil"
	"os"
	"strings"
)

// Bot Struct
type Bot struct {
	Token    string
	Commands []Command
	Banned   []string
}

type Command struct {
	// Must have
	Name        string
	Description string
	Response    string
	// Optional
	Guild   string
	Options []Option
}

type Option struct {
	Name        string
	Description string
	Type        uint
	Required    bool
}

// Reads in bot information from a JSON file
func read(fileName string) (Bot, error) {
	var newBot Bot

	// Open our jsonFile
	jsonFile, err := os.Open(fileName)
	if err != nil {
		newBot = Bot{Token: "error"}
		return newBot, err
	}

	// Print a success message
	fmt.Println("Successfully Opened", fileName)
	// defer the closing of our jsonFile so that we can parse it later on
	defer func(jsonFile *os.File) {
		err := jsonFile.Close()
		if err != nil {
			fmt.Println(err)
		}
	}(jsonFile)

	// read our opened xmlFile as a byte array.
	byteValue, _ := ioutil.ReadAll(jsonFile)

	err = json.Unmarshal(byteValue, &newBot)
	if err != nil {
		return Bot{}, err
	}

	return newBot, nil
}

// Loads commands in from a Bot, formats them and adds them to a client
func loadCommands(bot Bot, client *discordgo.Session) error {
	var com *discordgo.ApplicationCommand
	for _, command := range bot.Commands {
		com = createCommand(command)
		_, err := client.ApplicationCommandCreate(client.State.User.ID, command.Guild, com)
		if err != nil {
			return err
		}
	}

	return nil
}

func botInit(bot Bot) (*discordgo.Session, error) {
	// Create a new Discord session using the provided bot token.
	client, err := discordgo.New("Bot " + bot.Token)
	if err != nil {
		return nil, err
	}

	client.AddHandler(func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		err := respond(bot, s, i)
		if err != nil {
			return
		}
	})

	// Handles any message that is created
	client.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		err := messageRespond(bot, s, m)
		if err != nil {
			return
		}
	})

	// Adds handler to clear the commands (unless stated otherwise) when the bot receives the "ready" event
	client.AddHandler(ready)

	// Open a websocket connection to Discord and begin listening.
	err = client.Open()
	if err != nil {
		return nil, err
	}

	// Load Commands
	err = loadCommands(bot, client)
	if err != nil {
		return nil, err
	}

	return client, nil
}

// Is called when a "ready" event is received - used to clear commands
func ready(s *discordgo.Session, r *discordgo.Ready) {
	if Clear {
		err := clearCommands(s, r.Guilds)
		if err != nil {
			fmt.Println(err)
			return
		} else {
			fmt.Println("Commands cleared")
		}
	} else {
		fmt.Println("Commands weren't cleared")
	}
}

func clearCommands(s *discordgo.Session, guilds []*discordgo.Guild) error {
	// Add empty string for global commands
	guilds = append(guilds, &discordgo.Guild{
		ID: "",
	})

	// Go through each guild, get the commands and delete them
	for _, guild := range guilds {
		commands, err := s.ApplicationCommands(s.State.User.ID, guild.ID)
		if err != nil {
			return err
		}

		for _, command := range commands {
			err := s.ApplicationCommandDelete(s.State.User.ID, guild.ID, command.ID)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// Turns a Option struct into the correct format used by discordgo
func createOption(option Option) *discordgo.ApplicationCommandOption {
	op := &discordgo.ApplicationCommandOption{
		Name:        option.Name,
		Description: option.Description,
		Type:        discordgo.ApplicationCommandOptionType(option.Type),
		Required:    option.Required,
	}

	return op
}

// Turns a Command struct into the correct format used by discordgo
func createCommand(command Command) *discordgo.ApplicationCommand {
	if command.Options != nil {
		var ops []*discordgo.ApplicationCommandOption
		for _, option := range command.Options {
			ops = append(ops, createOption(option))
		}

		return &discordgo.ApplicationCommand{
			Name:        command.Name,
			Description: command.Description,
			Options:     ops,
		}
	} else {
		return &discordgo.ApplicationCommand{
			Name:        command.Name,
			Description: command.Description,
		}
	}
}

// Responds to a comment
func respond(bot Bot, client *discordgo.Session, interaction *discordgo.InteractionCreate) error {
	var response = ""
	var err error
	for _, command := range bot.Commands {
		if command.Name == interaction.Data.Name {
			if command.Options != nil {
				response = options(command, interaction)
			} else {
				response = command.Response
			}
		}
	}

	err = client.InteractionRespond(interaction.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionApplicationCommandResponseData{
			Content: response,
		},
	})
	if err != nil {
		return err
	}

	return nil
}

// Takes the value from the options in the command and formats them into the response string
func options(command Command, interaction *discordgo.InteractionCreate) string {
	response := command.Response

	for i, option := range command.Options {
		response = strings.Replace(response, "`"+option.Name+"`", interaction.Data.Options[i].StringValue(), 1)
	}

	return response
}

func messageRespond(bot Bot, s *discordgo.Session, m *discordgo.MessageCreate) error {
	// Ignores any messages created by the bot
	if m.Author.ID == s.State.User.ID {
		return nil
	}

	// Checks against array of banned words
	for _, current := range bot.Banned {
		if strings.Contains(strings.ToLower(m.Content), current) {
			if checkDelete(s, m.ChannelID) {
				_, err := s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("Message Removed\nReason: The word '%s' is banned", current))
				err = s.ChannelMessageDelete(m.ChannelID, m.ID)
				if err != nil {
					return err
				}
			} else {
				_, err := s.ChannelMessageSend(m.ChannelID, "Banned Word Detected but Bot does not have permissions to delete messages in this channel")
				if err != nil {
					return err
				}
			}
			break
		}
	}

	return nil
}

func checkDelete(s *discordgo.Session, channelID string) bool {
	p, err := s.State.UserChannelPermissions(s.State.User.ID, channelID)
	if err != nil {
		return false
	}
	return p&discordgo.PermissionAdministrator == discordgo.PermissionAdministrator ||
		p&discordgo.PermissionManageMessages == discordgo.PermissionManageMessages
}
