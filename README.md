# Go Discord Bot Controller

Small program to load in a discord bot from a JSON file, read its information and run the bot.

Currently, only works with basic bots that generate responses to commands (cannot process options as much as I want right now).

Even in this primitive state, you will probably need the basic knowledge of the Discord API and how to create a bot (https://discord.com/developers/docs/intro).

### How to Run
1. Make sure Go is installed (https://golang.org/doc/install)

2. Clone the repository using
`https://gitlab.com/Rory652/go-discord-bot-controller.git` (or SSH if you prefer)
   
3. Build the program using 
`go build`
   
4. Add JSON file to folder (or get path)
   
5. Run using:
    - Windows: `go-discord-bot-controller.exe -f /path/to/JSONfile.json`
    - Linux/Mac: `./go-discord-bot-controller -f /path/to/JSONfile.json`
    
6. Commands are automatically cleared when the bot starts (to remove any old commands no in the JSON). 
   If you don't want this to happen you can do so using the flag: `-clear=false` when running the program.
   
### Bot Permissions
To make sure that the bot can work correctly, you need to make sure that the "bot" and the "applications.commands" permissions are selected when creating and OAuth2 link.

It also helps to have the "Manage Channels" or "Administrator" permissions given to the bot in the server if you want it to be able to filter out certain words
    
### JSON File Format
The program requires a specific format for the JSON shown below.

Any time you use an option and want to add the returned value into the response use the format: \`field name\`

```json
{
  "token" :  "Bot Token Here",
  "commands" : [
    {"name": "Command 1", "description": "Global command without options", "response": "Command Response"},
    {"name": "Command 2", "description": "Guild specific command with options", "options" : [{"name" : "option name", "description" :  "what this does", "type" :  6, "required" : true}], "response" :  "This will display the response: `option name`", "guild":  "Guild ID"}],
  "banned" : ["filter", "words", "here", "in lower case"]
}
```

There are a few fields that are optional:
* **"options"**: 
    * This can allow you to take input in with each command and then display that in the response.
    * Options come in different types specified using the "type" field (different types listed here: https://discord.com/developers/docs/interactions/slash-commands#applicationcommandoptiontype)
    * Only options 3-8 will work properly for now, subcommands will be added at a later date along with more ways to process the options received.
    
* **"guild"**:
    * This allows you to add a command to a specific guild (server), leaving this blank will make the command global.
    * Global commands can take up to an hour to be added or removed - be patient!
    
There are some fields where you don't enter the response as a string (i.e. don't use quotation marks):
* **"type"**: this should be an integer.
* **"required"**: this should be a boolean (true or false)  - this doesn't really affect how the command is processed currently.

### Future Plans

I intend to create a tool that allows you to quickly and easily make these JSON files, but I want to finish adding a good amount of the features I have planned.

I also intend to allow people to run custom code for a command, but I need to figure out how to do that while making sure that it is secure. This will allow you to do more than just have text/images appear when using commands.